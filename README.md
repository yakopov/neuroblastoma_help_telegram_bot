# neuroblastoma_help_telegram_bot

A simple Telegram bot (@neuroblastoma_help_telegram_bot) for
[neuroblastoma.help](https://neuroblastoma.help) project.

Responses are defined in `responses.json` file.
