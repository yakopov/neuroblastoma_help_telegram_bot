import unittest
from unittest.mock import patch
import bot


class TestWeb(unittest.TestCase):

    def setUp(self):
        bot.app.config['TESTING'] = True
        bot.app.config['WTF_CSRF_ENABLED'] = False
        bot.app.config['DEBUG'] = False

        self.app = bot.app.test_client()

    def test_healthcheck(self):
        response = self.app.get('/healthcheck')

        self.assertEqual(response.status_code, 200)
        self.assertEqual(response.text, "OK")


class MockMessage:
    class MockChat:
       def __init__(self):
           self.id = "foo"

    def __init__(self, text: str):
        self.text = text
        self.chat = self.MockChat()


class TestTelebot(unittest.TestCase):
    @patch("bot.responses", None)
    def test_load_responses(self):
        responses = bot.load_responses()

        self.assertTrue(type(responses) is dict, "Responses not loaded from the local JSON file"),
        self.assertFalse("foo" in responses)

    @patch("bot.responses", {"foo": "bar"})
    def test_load_responses_cached(self):
        responses = bot.load_responses()

        self.assertDictEqual(responses, {"foo": "bar"})

    @patch("bot.load_responses")
    def test_get_response(self, mocked_load_responses):
        mocked_load_responses.return_value = {
            "foo": "bar",
            "foobar": ["foo", "bar"]
        }

        self.assertEqual("bar", bot.get_response("foo"), "Unexpected scalar string response")
        self.assertEqual("foo bar", bot.get_response("foobar"), "Unexpected string list response")

        with self.assertRaises(KeyError):
            bot.get_response("bar")

    @patch("bot.responses", {})
    @patch("bot.telebot.TeleBot.reply_to")
    def test_respond_unknown(self, mocked_reply_to):
        msg = MockMessage("foo")
        bot.respond(msg)
        mocked_reply_to.assert_called_with(msg, "Unknown command foo")

    @patch("bot.responses", {"foo": "bar"})
    @patch("bot.telebot.TeleBot.reply_to")
    def test_respond_known(self, mocked_reply_to):
        msg = MockMessage("foo")
        bot.respond(msg)
        mocked_reply_to.assert_called_with(msg, "bar")

    @patch("bot.responses", {"foo": "bar", "foobar": "none"})
    @patch("bot.types.InlineKeyboardMarkup.add")
    @patch("bot.telebot.TeleBot.send_message")
    def test_send_welcome(self, mocked_send_message, mocked_markup_add):
        msg = MockMessage("foo")
        bot.send_welcome(msg)

        self.assertEqual(mocked_markup_add.call_count, 2)

        self.assertEqual(mocked_markup_add.call_args_list[0][0][0].text, "foo")
        self.assertEqual(mocked_markup_add.call_args_list[1][0][0].text, "foobar")


if __name__ == "__main__":
    unittest.main()
