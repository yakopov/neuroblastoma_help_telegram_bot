import os
import telebot
from telebot import types
import json
from flask import Flask
import threading

BOT_TOKEN = os.environ.get("BOT_TOKEN")
bot = telebot.TeleBot(BOT_TOKEN)

responses = None

app = Flask(__name__)


def load_responses():
    global responses

    if responses is None:
        with open(os.path.join(os.path.dirname(__file__) + os.path.sep + "responses.json")) as f:
            return json.load(f)

    return responses


def get_response(command_key: str) -> str:
    responses = load_responses()

    if command_key not in responses:
        raise KeyError("Command not found in the response file")

    rsp = responses[command_key]

    if type(rsp) is list:
        return " ".join(rsp)

    return rsp


@bot.message_handler(commands=["about"])
def respond(message: types.Message):
    try:
        response_str = get_response(message.text)
    except KeyError:
        response_str = f"Unknown command {message.text}"

    bot.reply_to(message, response_str)


@bot.message_handler(commands=['start'])
def send_welcome(message: types.Message):
    options = list(load_responses().keys())
    markup = types.InlineKeyboardMarkup(row_width=len(options))

    for opt in options:
        button = types.InlineKeyboardButton(opt, callback_data=opt)
        markup.add(button)

    bot.send_message(message.chat.id, "Что вам подсказать?", reply_markup=markup)


@bot.callback_query_handler(func=lambda call: True)
def callback_handler(call):
    all_responses = load_responses()
    options = list(all_responses.keys())

    if call.data in options:
        bot.answer_callback_query(call.id, all_responses[call.data])


def start_flask_server():
    port = os.environ.get("WEB_PORT", 8080)
    app.run(host="0.0.0.0", port=port)


@app.route("/healthcheck")
def healthcheck():
    return "OK"


if __name__ == "__main__":
    threading.Thread(target=start_flask_server).start()

    bot.infinity_polling()
